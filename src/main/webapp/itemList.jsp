<%--
  Created by IntelliJ IDEA.
  User: wcmei
  Date: 4/7/2021
  Time: 12:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Rocket League Item List</title>
</head>
<body>
<h2 style="text-align: center">Sample Rocket League Item Database</h2>
<p style="text-align: center">This is a sample database where a user might want to <br>
    find simple information regarding a rocket league item price.  Through searching via different <br>
    methods on this web application, a user will be able to discover an items price in every variant.</p>

<center>
    <h3>
        <a href="index.jsp">Home</a>
        &nbsp;&nbsp;&nbsp;
        <a href="itemList.jsp">Show All Items</a>
    </h3>
</center>

<center>
    <h4>
        <!-- the href activates the "all" action in the servlet -->
        <a href="all">Show list of Items</a>
    </h4>
</center>
<center>
    <table>
        <tr>
            <th>Name</th>
            <th>Plain</th>
            <th>Black</th>
            <th>White</th>
            <th>Grey</th>
            <th>Crimson</th>
            <th>Pink</th>
            <th>Cobalt</th>
            <th>Blue</th>
            <th>Sienna</th>
            <th>Saffron</th>
            <th>Lime</th>
            <th>Green</th>
            <th>Orange</th>
            <th>Purple</th>

        </tr>

        <tr>
            <td>object.name</td>
            <td>object.plain</td>
            <td>object.black</td>
            <td>object.white</td>
            <td>object.grey</td>
            <td>object.crimson</td>
            <td>object.pink</td>
            <td>object.cobalt</td>
            <td>object.blue</td>
            <td>object.sienna</td>
            <td>object.saffron</td>
            <td>object.lime</td>
            <td>object.green</td>
            <td>object.orange</td>
            <td>object.purple</td>
        </tr>


    </table>
</center>
</body>
</html>
