package com.example;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.io.IOException;
import static org.jsoup.Jsoup.connect;

public class rlitems {


    public static void main(String[] args) throws IOException {

        try{
            final Document doc = connect("https://rl.insider.gg/en/pc").maxBodySize(0).timeout(600000)
                    .get();

            for(Element row : doc.select("table.priceTable tr")){
                if (row.select(".itemNameSpan").text().equals("")) {
                    continue;
                }
                else {
                    final String id = row.select(".itemNameSpan").text();
                    final String price = row.select(".priceRange.priceDefault").text();
                    final String bpaint = row.select(".priceRange.priceBlack").text();
                    final String whpaint = row.select(".priceRange.priceWhite").text();
                    final String gpaint = row.select(".priceRange.priceGrey").text();
                    final String crpaint = row.select(".priceRange.priceCrimson").text();
                    final String ppaint = row.select(".priceRange.pricePink").text();
                    final String copaint = row.select(".priceRange.priceCobalt").text();
                    final String skpaint = row.select(".priceRange.priceSkyBlue").text();
                    final String bspaint = row.select(".priceRange.priceBurntSienna").text();
                    final String spaint = row.select(".priceRange.priceSaffron").text();
                    final String lpaint = row.select(".priceRange.priceLime").text();
                    final String fgpaint = row.select(".priceRange.priceForestGreen").text();
                    final String opaint = row.select(".priceRange.priceOrange").text();
                    final String pupaint = row.select(".priceRange.pricePurple").text();
                    final String plain = price.replace("—","Not Available");
                    final String black = bpaint.replace("—","Not Available");
                    final String white = whpaint.replace("—","Not Available");
                    final String grey = gpaint.replace("—","Not Available");
                    final String crimson = crpaint.replace("—","Not Available");
                    final String pink = ppaint.replace("—","Not Available");
                    final String cobalt = copaint.replace("—","Not Available");
                    final String blue = skpaint.replace("—","Not Available");
                    final String sienna = bspaint.replace("—","Not Available");
                    final String saffron = spaint.replace("—","Not Available");
                    final String lime = lpaint.replace("—","Not Available");
                    final String green = fgpaint.replace("—","Not Available");
                    final String orange = opaint.replace("—","Not Available");
                    final String purple = pupaint.replace("—","Not Available");


                    System.out.println("\n\nItem Name: " + id + "\nItem Price: Default Color - " + plain + "\nBlack: " + black + "\nWhite: " + white + "\nGrey: " + grey + "\nCrimson: " + crimson + "\nPink: " + pink + "\nCobalt: " + cobalt + "\nSky Blue: " + blue + "\nBurnt Sienna: " + sienna + "\nSaffron: " + saffron + "\nLime: " + lime + "\nForest Green: " + green + "\nOrange: " + orange + "\nPurple: " + purple);
                }
            }
        }catch (Exception d){
            d.printStackTrace();
        }


    }
}