package com.example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.List;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ItemDAO{
    SessionFactory factory = null;
    Session pls1 = null;

    private static ItemDAO single_instance = null;

    ItemDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    public static ItemDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new ItemDAO();
        }

        return single_instance;
    }

    public List<itemobject> getItems(){
        try{
            pls1 = factory.openSession();
            pls1.getTransaction().begin();
            String sql = "from com.example.itemobject";
            List<itemobject> dk = (List<itemobject>)pls1.createQuery(sql).getResultList();
            pls1.getTransaction().commit();
            return dk;
        }catch (Exception z){
            z.printStackTrace();
            pls1.getTransaction().rollback();
            return null;
        } finally{
            pls1.close();
        }
    }




        /**try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.itemobject";
            List<itemobject> cs = (List<itemobject>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single customer from database
    public itemobject getItems(String name) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.itemobject where id= " + name;
            itemobject c = (itemobject) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }*/



}

