package com.example;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "itemEnter", value = "/itemEnter")
public class itemEnter extends HttpServlet{
    private ItemDAO DAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ServletException {
        ItemEnter(req, resp, "GET");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ItemEnter(request, response, "POST");
    }

    protected void ItemEnter(HttpServletRequest request, HttpServletResponse response, String requestType) throws IOException {
        System.out.println("Enter an item name: " + requestType);

        DAO = ItemDAO.getInstance();
        String name = request.getParameter("name");
        DAO.getItems();
        response.setContentType("text/html");
        try {
            PrintWriter respond = response.getWriter();
            respond.println("<!DOCTYPE html><html lang=\"en-us\"><head>");
            respond.println("<title> Enter an Item</title>");
            respond.println("</head><body>");
            respond.println("<h1>Enter an Item</h1>");
            respond.print("<form method=\"POST\" action=\"EnterPokemon\">");
            respond.println("Item Name: ");
            respond.println("<input type=text name=name/>");
            respond.println("Item: ");
            respond.println("<input type=text name=type/>");
            respond.println("<input type=\"submit\" value=\"Save\" />");
            respond.println("</form>");
            respond.println("<br>");
            respond.println("<a href=\"itemSearch\"><button type=\\\"button\\\">Return to Item List</button/></a>");
            respond.println("</br>");
            respond.println("</body>");
        } catch(IOException e){
            e.printStackTrace();
            response.sendError(1, "Something went wrong");
        }
    }
}
