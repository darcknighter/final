package com.example;


import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "itemSearch", value = "/itemSearch")
public class itemSearch extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ServletException{

        try {
            makeResponse(req, resp, "GET");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            makeResponse(request, response, "POST");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    private void makeResponse(HttpServletRequest req, HttpServletResponse resp, String requestType) throws ServletException, IOException, SQLException {
        System.out.println(requestType + ": Items");
        ItemDAO DAO = ItemDAO.getInstance();
        List<itemobject> i = DAO.getItems();
        resp.setContentType("text/html");
        try{
         PrintWriter respond = resp.getWriter();
         respond.println("<!DOCTYPE html><html lang=\\\"en-us\\\"><head>");
         respond.println("<title>Items</title>");
         respond.println("</head><body>");
         respond.println("<div>");
         respond.println("<a href=\"itemEnter\" New Item Entry</a>");
         respond.println("<a href=\"index.jsp\" Home Page</a>");
         respond.println("</div>");
         respond.println("<h1>Item List</h1>");
         respond.println("<table class=\"table table-sm table-bordered\">");
         respond.println("<tr>");
         respond.println("<th>Item Name</th>");
         respond.println("<th class-\"text-right\">type</th>");
         respond.println("</tr>");
            for (itemobject p: i){
            respond.println("<tr>");
            respond.println("<td>" + p.getName() + "</td>");
            respond.println("<td>" + p.getWhite() + "</td>");
         respond.println("<td>" + p.getPlain() + "</td>");
         respond.println("<td>" + p.getBlack() + "</td>");
         respond.println("<td>" + p.getPink() + "</td>");
         respond.println("<td>" + p.getPurple() + "</td>");
         respond.println("<td>" + p.getBlue() + "</td>");
         respond.println("<td>" + p.getGreen() + "</td>");
         respond.println("<td>" + p.getGrey() + "</td>");
         respond.println("<td>" + p.getOrange() + "</td>");
         respond.println("<td>" + p.getSaffron() + "</td>");
         respond.println("<td>" + p.getSienna() + "</td>");
         respond.println("<td>" + p.getCobalt() + "</td>");
         respond.println("<td>" + p.getCrimson() + "</td>");
         respond.println("<td>" + p.getLime() + "</td>");
         respond.println("</tr>");
         }
         respond.println("<div>");
         respond.println("<br><a href=\"index.jsp\"><button type=\"button\">Enter</button/></a>");
         respond.println("</div>");
         respond.println("</body></html>");
         }catch (IOException e){
         e.printStackTrace();
         }
         }

    /**private void listAll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, ServletException {
        List <itemobject> listAll = DAO.getItems();
        request.setAttribute("listobject", listAll);
        RequestDispatcher dispatcher = request.getRequestDispatcher("itemList.jsp");
        dispatcher.forward(request, response);
    }*/

}
