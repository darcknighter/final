package com.example;
import javax.persistence.*;


@Entity
@Table(name = "itemobject")
public class itemobject {
    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "plain")
    private String plain;

    @Column(name = "black")
    private String black;

    @Column(name = "white")
    private String white;

    @Column(name = "grey")
    private String grey;

    @Column(name = "crimson")
    private String crimson;

    @Column(name = "pink")
    private String pink;

    @Column(name = "cobalt")
    private String cobalt;

    @Column(name = "blue")
    private String blue;

    @Column(name = "sienna")
    private String sienna;

    @Column(name = "saffron")
    private String saffron;

    @Column(name = "lime")
    private String lime;

    @Column(name = "green")
    private String green;

    @Column(name = "orange")
    private String orange;

    @Column(name = "purple")
    private String purple;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getPlain(){
        return plain;
    }

    public void setPlain(String plain){
        this.plain = plain;
    }

    public String getBlack(){
        return black;
    }

    public void setBlack(String black){
        this.black = black;
    }

    public String getWhite(){
        return white;
    }

    public void setWhite(String white){
        this.white = white;
    }

    public String getGrey(){
        return grey;
    }

    public void setGrey(String grey){
        this.grey = grey;
    }

    public String getCrimson(){
        return crimson;
    }

    public void setCrimson(String crimson){
        this.crimson = crimson;
    }

    public String getPink(){
        return pink;
    }

    public void setPink(String pink){
        this.pink = pink;
    }

    public String getCobalt(){
        return cobalt;
    }

    public void setCobalt(String cobalt){
        this.cobalt = cobalt;
    }

    public String getBlue(){
        return blue;
    }

    public void setBlue(String blue){
        this.blue = blue;
    }

    public String getSienna(){
        return sienna;
    }

    public void setSienna(String sienna){
        this.sienna = sienna;
    }

    public String getSaffron(){
        return saffron;
    }

    public void setSaffron(String saffron){
        this.saffron = saffron;
    }

    public String getLime(){
        return lime;
    }

    public void setLime(String lime){
        this.lime = lime;
    }

    public String getGreen(){
        return green;
    }

    public void setGreen(String green){
        this.green = green;
    }

    public String getOrange(){
        return orange;
    }

    public void setOrange(String orange){
        this.orange = orange;
    }

    public String getPurple(){
        return purple;
    }

    public void setPurple(String purple){
        this.purple = purple;
    }

    public String toString() {
        return id + "\nItem Name: " + name + "\nItem Price: Default Color - " + plain + "\nBlack: " + black + "\nWhite: " + white + "\nGrey: " + grey + "\nCrimson: " + crimson + "\nPink: " + pink + "\nCobalt: " + cobalt + "\nSky Blue: " + blue + "\nBurnt Sienna: " + sienna + "\nSaffron: " + saffron + "\nLime: " + lime + "\nForest Green: " + green + "\nOrange: " + orange + "\nPurple: " + purple;
    }
}


