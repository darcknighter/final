package com.example;
import java.util.*;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

import static org.jsoup.Jsoup.connect;

public class connect {
    public static SessionFactory factory = new Configuration().configure().buildSessionFactory();

    private static ItemDAO single_instance = null;

    void ItemDAO() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static ItemDAO getInstance() {
        if (single_instance == null) {
            single_instance = new ItemDAO();
        }
        return single_instance;
    }

    public static void main(String[] args) {


        Session session = null;

        try {
            final Document doc = connect("https://rl.insider.gg/en/pc").maxBodySize(0).timeout(600000)
                    .get();

            for (Element row : doc.select("table.priceTable tr")) {
                if (row.select(".itemNameSpan").text().equals("")) {
                    continue;
                } else {
                    final String name = row.select(".itemNameSpan").text();
                    final String price = row.select(".priceRange.priceDefault").text();
                    final String bpaint = row.select(".priceRange.priceBlack").text();
                    final String whpaint = row.select(".priceRange.priceWhite").text();
                    final String gpaint = row.select(".priceRange.priceGrey").text();
                    final String crpaint = row.select(".priceRange.priceCrimson").text();
                    final String ppaint = row.select(".priceRange.pricePink").text();
                    final String copaint = row.select(".priceRange.priceCobalt").text();
                    final String skpaint = row.select(".priceRange.priceSkyBlue").text();
                    final String bspaint = row.select(".priceRange.priceBurntSienna").text();
                    final String spaint = row.select(".priceRange.priceSaffron").text();
                    final String lpaint = row.select(".priceRange.priceLime").text();
                    final String fgpaint = row.select(".priceRange.priceForestGreen").text();
                    final String opaint = row.select(".priceRange.priceOrange").text();
                    final String pupaint = row.select(".priceRange.pricePurple").text();
                    final String plain = price.replace("—", "Not Available");
                    final String black = bpaint.replace("—", "Not Available");
                    final String white = whpaint.replace("—", "Not Available");
                    final String grey = gpaint.replace("—", "Not Available");
                    final String crimson = crpaint.replace("—", "Not Available");
                    final String pink = ppaint.replace("—", "Not Available");
                    final String cobalt = copaint.replace("—", "Not Available");
                    final String blue = skpaint.replace("—", "Not Available");
                    final String sienna = bspaint.replace("—", "Not Available");
                    final String saffron = spaint.replace("—", "Not Available");
                    final String lime = lpaint.replace("—", "Not Available");
                    final String green = fgpaint.replace("—", "Not Available");
                    final String orange = opaint.replace("—", "Not Available");
                    final String purple = pupaint.replace("—", "Not Available");
                    try {
                        itemobject item = new itemobject();
                        item.setName(name);
                        item.setPlain(plain);
                        item.setBlack(black);
                        item.setWhite(white);
                        item.setGrey(grey);
                        item.setCrimson(crimson);
                        item.setPink(pink);
                        item.setCobalt(cobalt);
                        item.setBlue(blue);
                        item.setSienna(sienna);
                        item.setSaffron(saffron);
                        item.setLime(lime);
                        item.setGreen(green);
                        item.setOrange(orange);
                        item.setPurple(purple);

                        session = factory.openSession();
                        session.beginTransaction();
                        session.save(item);
                        session.getTransaction().commit();
                    } catch (Exception f) {
                        f.printStackTrace();
                        session.getTransaction().rollback();
                    } finally {
                        session.close();
                    }
                    ItemDAO g = ItemDAO.getInstance();
                    List<itemobject> z = g.getItems();
                    for (itemobject i : z) {
                        System.out.println(i);
                    }
                }

            }
        } catch(InputMismatchException e){
            System.out.println("Try to make sure you fill it out exactly as requested.");
        } catch(NumberFormatException e){
            System.out.println("Enter a number when i ask for a number. Pretty simple dude.");
        } catch (Exception d) {
            d.printStackTrace();
        } finally{
            System.out.println("Why are GAME SKINS so dang expensive?!?!");

        }

    }
}
